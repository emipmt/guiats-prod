<?php

require_once '../conn.php';
require_once '../middleware.php';


$work = $_POST['work'];
$idUser = $_POST['idUser'];
$password = $_POST['password'];

$res = verify("client", $idUser, $conn);
if(!$res["status"]) {
	$res["message"] = "No tienes acceso";
	echo json_encode($res);
	return;
}


$sqlFindAccount = "SELECT * FROM participants WHERE work = {$work} and idUser = {$idUser}";
$resultFindAccount = mysqli_query($conn, $sqlFindAccount);

if (mysqli_num_rows($resultFindAccount) > 0) {

  $row = mysqli_fetch_assoc($resultFindAccount);

  if ($row["password"] == $password) {

		if($row["resolved"]){
			$response->status = false;
			$response->message = "Ya has resuelto el cuestionario";
		} else {
			$data->idParticipant = $row["idParticipant"];
			$data->resolved = $row["resolved"];
			$data->work = $row["work"];
			$data->name = $row["name"];
			$data->idUser = $row["idUser"];

			$response->status = true;
			$response->message = "Bienvenido";
			$response->participantData = $data;
		}


		echo json_encode($response, JSON_NUMERIC_CHECK);
  } else {

	$response->status = false;
	$response->message = "Número de trabajador o contraseña incorrectos";
	echo json_encode($response);

  }

} else {

  $response->status = false;
  $response->message = "No se encontró una cuenta con ese número";
  echo json_encode($response);

}


?>