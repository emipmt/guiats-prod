<?php
require_once '../conn.php';
require_once '../middleware.php';

$idParticipant = $_POST["idParticipant"];
$idUser = $_POST["idUser"];
$options = $_POST["options"];
$res = verify("client", $idUser, $conn);
if(!$res["status"]) {
	echo json_encode($res);
	return;
}

foreach($options as $idOption){
	$sql = "insert into selected (idoption, idParticipant) values ({$idOption}, {$idParticipant})";
	if (mysqli_query($conn, $sql)) {
		$response->status = true;
		$response->message = "Se ha creado correctamente";
	} else {
		$response->status = false;
		$response->message = "Ha ocurrido un error al crear";
	}
}

echo json_encode($response);
