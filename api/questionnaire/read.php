<?php
require_once '../conn.php';

$idParticipant = $_POST["idParticipant"];

$sql = "Select a.answer, r.title, r.idReactive, r.idAspect from answers a, reactives r where a.idParticipant={$idParticipant} and r.idReactive=a.idReactive";
$result = mysqli_query($conn, $sql);
$questionnaire = array();
while ($row = mysqli_fetch_assoc($result)) {
    array_push($questionnaire,$row);
}

$response->questionnaire = $questionnaire;
$response->status = true;
echo json_encode($response, JSON_NUMERIC_CHECK);


//Ejemplo de consulta
// $sql = "SELECT * from participants p, selected s, option o, personaldataoptions pd
// WHERE p.idParticipant = s.idParticipant
// AND s.idOption = o.idOption
// AND o.idPersonalDataOptions = pd.idPersonalDataOptions
// AND p.idParticipant = 2";