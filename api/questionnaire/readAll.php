<?php
require_once '../conn.php';

$idUser = $_POST["idUser"];
    
$sql = "select idParticipant from participants where idUser={$idUser}";
$result = mysqli_query($conn, $sql);
$participants = array();
while ($row = mysqli_fetch_assoc($result)) {

    $sqlQuestionnaire = "Select a.answer, r.idReactive, r.idAspect from answers a, reactives r where a.idParticipant={$row['idParticipant']} and r.idReactive=a.idReactive";
    $resultQuestionnaire = mysqli_query($conn, $sqlQuestionnaire);
    $questionnaire = array();
    while ($rowQuestionnaire = mysqli_fetch_assoc($resultQuestionnaire)) {
        array_push($questionnaire,$rowQuestionnaire);

    }

    $row["questionnaire"] = $questionnaire;

    array_push($participants,$row);

}


$response->participants = $participants;
$response->status = true;
echo json_encode($response, JSON_NUMERIC_CHECK);


