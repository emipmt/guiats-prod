<?php
require_once '../conn.php';

$idUser = $_GET["idUser"];

$sql = "SELECT idUser, name, orgName, email, phone FROM users where idUser = {$idUser}";
$result = mysqli_query($conn, $sql);
$data = new \stdClass();

if (mysqli_num_rows($result) > 0) {

	$response->status = true;
	$response->data =  mysqli_fetch_assoc($result);
	echo json_encode($response, JSON_NUMERIC_CHECK);	
} else {
	$response->status = false;
	$response->data = $data;
	$response->message = "Aún no hay usuarios en la plataforma";
	echo json_encode($response);
}
