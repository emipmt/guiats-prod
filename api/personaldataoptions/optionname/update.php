<?php

require_once '../../conn.php';
require_once '../../middleware.php';

$idOption = $_POST["idOption"];
$name = $_POST["name"];
$idUserVerify = $_POST["idUserVerify"];
$res = verify("admin", $idUserVerify, $conn);
if(!$res["status"]) {
	echo json_encode($res);
	return;
}


$sql = "update option set name='{$name}' where idOption = {$idOption}";

if (mysqli_query($conn, $sql)) {
	$response->status = true;
	$response->message = "Actualizado correctamente";
	echo json_encode($response);

} else {
	$response->status = false;
	$response->message = "Ha ocurrido un error al actualizar dato, intentalo más tarde.";
	echo json_encode($response);

}

?>