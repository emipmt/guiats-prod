<?php

require_once '../../conn.php';
require_once '../../middleware.php';

$optionName = $_POST["optionName"];
$idPersonalDataOptions = $_POST["idPersonalDataOptions"];
$idUser = $_POST["idUser"];
$idUserVerify = $_POST["idUserVerify"];
$res = verify("admin", $idUserVerify, $conn);
if(!$res["status"]) {
	echo json_encode($res);
	return;
}


$sql = "insert into option (name, idPersonalDataOptions, idUser) values ('{$optionName}', {$idPersonalDataOptions}, {$idUser})";

if (mysqli_query($conn, $sql)) {
	$response->status = true;
	$response->message = "Creado correctamente";
	echo json_encode($response);

} else {
	$response->status = false;
	$response->message = "Ha ocurrido un error al crear dato, intentalo más tarde.";
	echo json_encode($response);

}

?>