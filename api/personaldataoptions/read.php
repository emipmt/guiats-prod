<?php
require_once '../conn.php';

$idUser = $_POST["idUser"];

$sql = "SELECT * FROM personaldataoptions";
$result = mysqli_query($conn, $sql);
$data = array();

if (mysqli_num_rows($result) > 0) {

	while ($row = mysqli_fetch_assoc($result)) {
		//extraer name de option

		$row["options"] = array();
		$sqlOptions = "SELECT * FROM option where idUser = {$idUser} and idPersonalDataOptions = {$row['idPersonalDataOptions']}";
		$resultOptions = mysqli_query($conn, $sqlOptions);

		while ($rowOptions = mysqli_fetch_assoc($resultOptions)) {
			array_push($row["options"], $rowOptions);
		}

		array_push($data, $row);
	}

	$response->status = true;
	$response->data = $data;
	echo json_encode($response, JSON_NUMERIC_CHECK);
} else {
	$response->status = false;
	$response->data = $data;
	$response->message = "Aún no hay usuarios en la plataforma";
	echo json_encode($response);
}
