<?php
require_once '../conn.php';
require_once '../middleware.php';

$idParticipant = $_POST["idParticipant"];
$answers = $_POST["answers"];
$idReactives = $_POST["idReactives"];
$idUser = $_POST["idUser"];
$date = $_POST["date"];
$res = verify("client", $idUser, $conn);

if(!$res["status"]) {
	echo json_encode($res);
	return;
}

for($i = 0; $i < count($answers); $i++){
	$sql = "insert into answers (answer, idReactive, idParticipant) values ({$answers[$i]}, {$idReactives[$i]}, {$idParticipant})";

	if (mysqli_query($conn, $sql)) {
		$response->status = true;
		$response->message = "Se ha guardado correctamente";
	} else {
		$response->status = false;
		$response->message = "Ha ocurrido un error al crear la respuesta";
	}

}

$sql = "update participants set resolved='{$date}' where idParticipant={$idParticipant}";

if (mysqli_query($conn, $sql)) {
	$response->status = true;
	$response->message = "Se ha guardado correctamente";
	echo json_encode($response);
} else {
	$response->status = false;
	$response->message = "Ha ocurrido un error al actualizar el empleado";
	echo json_encode($response);
}
