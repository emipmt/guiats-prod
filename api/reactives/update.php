<?php

require_once '../conn.php';
require_once '../middleware.php';

$idReactive = $_POST["idReactive"];
$title = $_POST["title"];
$timer = $_POST["timer"];
$idAspect = $_POST["idAspect"];
$idUserVerify = $_POST["idUserVerify"];
$res = verify("admin", $idUserVerify, $conn);
if(!$res["status"]) {
	echo json_encode($res);
	return;
}


$sql = "update reactives set title='{$title}', timer={$timer}, idAspect={$idAspect} where idReactive = {$idReactive}";

if (mysqli_query($conn, $sql)) {
	$response->status = true;
	$response->message = "Actualizado correctamente";
	echo json_encode($response);

} else {
	$response->status = false;
	$response->message = "Ha ocurrido un error al actualizar dato, intentalo más tarde.";
	echo json_encode($response);

}

?>