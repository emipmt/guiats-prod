<?php

require_once '../conn.php';
require_once '../middleware.php';

$title = $_POST["title"];
$timer = $_POST["timer"];
$idAspect = $_POST["idAspect"];
$idUserVerify = $_POST["idUserVerify"];
$res = verify("admin", $idUserVerify, $conn);
if(!$res["status"]) {
	echo json_encode($res);
	return;
}


$sql = "insert into reactives (title, timer, idAspect) values ('{$title}', {$timer}, {$idAspect})";

if (mysqli_query($conn, $sql)) {
	$response->status = true;
	$response->message = "Creado correctamente";
	echo json_encode($response);

} else {
	$response->status = false;
	$response->message = "Ha ocurrido un error al crear dato, intentalo más tarde.";
	echo json_encode($response);

}

?>