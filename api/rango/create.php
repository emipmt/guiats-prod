<?php

require_once '../conn.php';
require_once '../middleware.php';

$rango = $_POST["rango"];
$minRango = $_POST["minRango"];
$maxRango = $_POST["maxRango"];
$diagnosis = $_POST["diagnosis"];
$recommendation = $_POST["recommendation"];
$idAspect = $_POST["idAspect"];

$idUserVerify = $_POST["idUserVerify"];
$res = verify("admin", $idUserVerify, $conn);
if(!$res["status"]) {
	echo json_encode($res);
	return;
}


$sql = "insert into rango (rango, minRango, maxRango, diagnosis, recommendation, idAspect) values
 ({$rango}, {$minRango}, {$maxRango}, '{$diagnosis}', '{$recommendation}', {$idAspect})";

if (mysqli_query($conn, $sql)) {
	$response->status = true;
	$response->message = "Creado correctamente";
	echo json_encode($response);

} else {
	$response->status = false;
	$response->message = "Ha ocurrido un error al crear dato, intentalo más tarde.";
	echo json_encode($response);

}

?>