<?php

require_once '../conn.php';
require_once '../middleware.php';

$idRango = $_POST["idRango"];
$rango = $_POST["rango"];
$minRango = $_POST["minRango"];
$maxRango = $_POST["maxRango"];
$diagnosis = $_POST["diagnosis"];
$recommendation = $_POST["recommendation"];
$idUserVerify = $_POST["idUserVerify"];
$res = verify("admin", $idUserVerify, $conn);
if(!$res["status"]) {
	echo json_encode($res);
	return;
}

$sql = "update rango set rango={$rango}, minRango={$minRango}, maxRango={$maxRango},
diagnosis='{$diagnosis}', recommendation='{$recommendation}' where idRango = {$idRango}";


if (mysqli_query($conn, $sql)) {
	$response->status = true;
	$response->message = "Actualizado correctamente";
	echo json_encode($response);

} else {
	$response->status = false;
	$response->message = "Ha ocurrido un error al actualizar dato, intentalo más tarde.";
	echo json_encode($response);

}

?>