<?php

  require_once '../conn.php';
  require_once '../middleware.php';

  	$idRango = $_POST['idRango'];
	$idUserVerify = $_POST["idUserVerify"];
	$res = verify("admin", $idUserVerify, $conn);
	if(!$res["status"]) {
		echo json_encode($res);
		return;
	}

  $sql = "delete from rango where idRango = {$idRango}";
  if (mysqli_query($conn, $sql)) {
    $response->status = true;
    $response->message = "Se ha eliminado correctamente";
    echo json_encode($response);
  } else {
    $response->status = false;
    $response->message = "Ha ocurrido un error al eliminar";
    echo json_encode($response);
  }
