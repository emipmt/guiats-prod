<?php
  require_once '../conn.php';

    $email = $_POST["email"];
    $hash = $_POST["hash"];
    $password = password_hash($_POST['password'],PASSWORD_DEFAULT);

    if (md5($email) == $hash) {

        $sql = "update users set password={$password} where email = {$email}";

        if (mysqli_query($conn, $sql)) {
           $response->status = true;
           $response->message = "Contraseña Actualizada satisfactoriamente";
           echo json_encode($response);
        } else{
           $response->status = false;
           $response->message = "Ha ocurrido un error al intentar actualizar la contraseña";
           echo json_encode($response);
        }

    } else {

        $response->status = false;
        $response->message = "El correo electrónico es incorrecto";
        echo json_encode($response);

    }