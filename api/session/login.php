<?php

require_once '../conn.php';

$email = $_POST['email'];
$password = $_POST['password'];


$sqlFindAccount = "SELECT * FROM users WHERE email = '{$email}' ";
$resultFindAccount = mysqli_query($conn, $sqlFindAccount);


if (mysqli_num_rows($resultFindAccount) > 0) {

  $rowFindAccount = mysqli_fetch_assoc($resultFindAccount);

  if (password_verify($password,$rowFindAccount['password'])) {

		$data->id = $rowFindAccount['idUser'];
		$data->name = $rowFindAccount["name"];
		$data->orgName = $rowFindAccount['orgName'];
		$data->email = $email;
		$data->phone = $rowFindAccount['phone'];
		$data->type = $rowFindAccount['type'];
		$data->active = $rowFindAccount['active'];
		$data->maxParticipants = $rowFindAccount['maxParticipants'];

		$response->status = true;
	  	$response->message = "Bienvenido";
		$response->userData = $data;

		echo json_encode($response, JSON_NUMERIC_CHECK);
  } else {

	$response->status = false;
	$response->message = "Correo electrónico o contraseña incorrectos";
	echo json_encode($response);

  }

} else {

  $response->status = false;
  $response->message = "No se encontró una cuenta registrada con ese correo electrónico";
  echo json_encode($response);

}


?>
