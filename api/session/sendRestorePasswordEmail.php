<?php
  require_once '../conn.php';

$email = $_POST["email"];
$emailHash = md5($email);

if (sendEmail($email,$emailHash)) {

    $response->status = true;
    $response->message = "Se ha enviado el mensaje correctamente";
    echo json_encode($response);

} else {
    $response->status = false;
    $response->message = "Ha ocurrido un error al intentar enviar el mensaje";
    echo json_encode($response);
}


function sendEmail ($email, $hash) {
	//guiats.human-express.com/#/cuestionario/${idCliente}/${numeroDeTrabajador}/${contraseña}
    $message = "
   
   <div style='max-width:100vw'>
   <div style='background: url(https://guiats.human-express.com/img/background.e380e90d.png); background-size:100%;background-position:center; width:100%;padding:5% 0'>
      .
      <div style='text-align:center;'>
         <img src='https://guiats.human-express.com/img/logo.1b957308.png' style='width:100px;' />
      </div>
   </div>
   <div style='padding:5%;'>
      <p>
        Recibiste este correo electrónico porque solicitaste restablecer tu contraseña para el correo {$email} de tu cuenta GUIATS en Human Express. <br>
        Has click en el enlace de abajo para reestablecer tu contraseña
        
        <br>

        <a href='https://guiats.human-express.com/#/restorepassword/{$hash}'>https://guiats.human-express.com/#/restorepassword/{$hash}</a>

      </p>
   </div>
";

    $message = utf8_decode($message);

    $title = "Restablece tu clave de GUIATS en Human Express";
    
    $title = "=?ISO-8859-1?B?".base64_encode($title)."=?=";
    $headers = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
    $headers .= "From: dsaavedra@human-express.com";

    $mail = mail($email,$title,$message,$headers);
    if($mail){
      return true;
    } else{
      return false;
    }
  };