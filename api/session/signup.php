<?php

  require_once '../conn.php';

  $name = $_POST['name'];
  $orgName = $_POST["orgName"];
  $email = $_POST['email'];
  $password = password_hash($_POST['password'],PASSWORD_DEFAULT);
  $phone = $_POST["phone"];

  $sqlVerifyAccount = "SELECT * FROM users WHERE email = '{$email}' ";
  $resultVerifyAccount = mysqli_query($conn, $sqlVerifyAccount);

  if (mysqli_num_rows($resultVerifyAccount) > 0) {

   $rowVerifyAccount = mysqli_fetch_assoc($resultVerifyAccount);

   $response->status = false;
   $response->message = "Ya se está utilizando correo electrónico en otra cuenta";
   echo json_encode($response);

  } else {

    $sqlCreateAccount = "INSERT INTO users (name, orgName, email, password, phone, type, active)
    VALUES ('{$name}', '{$orgName}', '{$email}', '{$password}', '{$phone}', 0, 0)";
    if (mysqli_query($conn, $sqlCreateAccount)) {

      $sqlGetId = "SELECT * FROM users WHERE email = '{$email}' ";
      $resultGetId = mysqli_query($conn, $sqlGetId);
      if (mysqli_num_rows($resultGetId) > 0) {

		$rowFindAccount = mysqli_fetch_assoc($resultGetId);

		$data->id = $rowFindAccount['idUser'];
		$data->name = $rowFindAccount["name"];
		$data->orgName = $rowFindAccount['orgName'];
		$data->email = $email;
		$data->phone = $rowFindAccount['phone'];
		$data->type = $rowFindAccount['type'];
		$data->active = $rowFindAccount['active'];

        $response->status = true;
        $response->message = "Te has registrado con éxito, tu cuenta será activada pronto.";
        $response->userData = $data;
        echo json_encode($response, JSON_NUMERIC_CHECK);

      } else {

        $response->status = false;
        $response->message = "Ha ocurrido un error al obtener tus datos.";
        echo json_encode($response);

      }

    } else {

      $response->status = false;
      $response->message = "Ha ocurrido un error al crear, cuenta intentalo más tarde.";
      echo json_encode($response);

    }

  }


?>
