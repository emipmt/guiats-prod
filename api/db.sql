create database guiats default character set utf8 collate utf8_general_ci;
use guiats;

create table users(
	idUser int not null primary key auto_increment,
	name varchar(100) not null,
	orgName varchar(60) not null,
	email varchar(100) not null,
	password varchar(73) not null,
	phone varchar(30) not null,
	maxParticipants int not null default "0",
	type tinyint(1) not null,
	active tinyint(1) not null
);

create table personaldataoptions(
	idPersonalDataOptions int not null primary key auto_increment,
	optionType varchar(30) not null,
	optionName varchar(30) not null
);

create table option(
	idOption int not null primary key auto_increment,
	name varchar(100) not null,
	idPersonalDataOptions int not null,
	idUser int not null,
	foreign key(idPersonalDataOptions) references personalDataOptions(idPersonalDataOptions)
	on delete cascade on update cascade,
	foreign key(idUser) references users(idUser)
	on delete cascade on update cascade

);

create table settings(
	idSetting int not null primary key auto_increment,
	type varchar(30) not null,
	content text not null
);

create table participants(
	idParticipant int not null primary key auto_increment,
	resolved tinyint(1) not null default 0,
	emailSended tinyInt(1) not null default 0,
	work varchar(8) not null,
	name varchar(100) not null,
	email varchar(100) not null default "0",
	position varchar(100) not null,
	password varchar(73),
	constraint uq_participant unique(work, idUser),
	idUser int not null,
	foreign key(idUser) references users(idUser)
	on delete cascade on update cascade
);

create table selected(
	idSelected int not null primary key auto_increment,
	idOption int not null,
	idParticipant int not null,
	foreign key(idOption) references option(idOption)
	on delete cascade on update cascade,
	foreign key(idParticipant) references participants(idParticipant)
	on delete cascade on update cascade
);

create table aspects(
	idAspect int not null primary key auto_increment,
	title varchar(60) not null
);

create table rango(
	idRango int not null primary key auto_increment,
	rango int not null,
	minRango int not null,
	maxRango int not null,
	diagnosis text not null,
	recommendation text not null,
	idAspect int not null,
	foreign key(idAspect) references aspects(idAspect)
	on delete cascade on update cascade
);

create table reactives(
	idReactive int not null primary key auto_increment,
	title text not null,
	timer int not null default "0",
	idAspect int not null,
	foreign key(idAspect) references aspects(idAspect)
	on delete cascade on update cascade
);

create table answers(
	idAnswer int not null primary key auto_increment,
	answer tinyint(1) not null,
	idReactive int not null,
	idParticipant int not null,
	foreign key(idReactive) references reactives(idReactive)
	on delete cascade on update cascade,
	foreign key(idParticipant) references participants(idParticipant)
	on delete cascade on update cascade
);

insert into personaldataoptions (optionType, optionName) values
("entity", "Entidad"),
("area", "Área"),
("turn", "Turno"),
("gender", "Género"),
("ageRange", "Rango de edad"),
("antiquety", "Antigüedad"),
("study", "Estudios"),
("turnRotate", "Rotación de turno"),
("experience", "Experiencia en puesto ocupado"),
("totalEperience", "Tiempo de experiencia laboral"),
("categories", "Tipo de puesto"),
("typeContract", "Tipo de contrato");
("position", "Tipo de personal");