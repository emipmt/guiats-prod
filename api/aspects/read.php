<?php
require_once '../conn.php';

$sql = "SELECT * FROM aspects";
$result = mysqli_query($conn, $sql);
$data = array();

if (mysqli_num_rows($result) > 0) {

	while($row = mysqli_fetch_assoc($result)) {
		//extraer name de option
		$row["rangos"] = array();
		$sqlOptions = "SELECT * FROM rango where idAspect = {$row["idAspect"]}";
		$resultOptions = mysqli_query($conn, $sqlOptions);

		while($rowOptions = mysqli_fetch_assoc($resultOptions)) {
			array_push($row["rangos"], $rowOptions);
		}
		array_push($data,$row);
	}

	$response->status = true;
	$response->data = $data;
	echo json_encode($response, JSON_NUMERIC_CHECK);
} else {
	$response->status = false;
	$response->data = $data;
	$response->message = "Aún no hay usuarios en la plataforma";
	echo json_encode($response);
}
