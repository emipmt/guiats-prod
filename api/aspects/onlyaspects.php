<?php
require_once '../conn.php';

$sql = "SELECT * FROM aspects";
$result = mysqli_query($conn, $sql);
$data = array();

if (mysqli_num_rows($result) > 0) {

	while($row = mysqli_fetch_assoc($result)) {
		array_push($data,$row);
	}

	$response->status = true;
	$response->data = $data;
	echo json_encode($response, JSON_NUMERIC_CHECK);
} else {
	$response->status = false;
	$response->data = $data;
	$response->message = "Aún no hay aspectos en la plataforma";
	echo json_encode($response);
}
