<?php
  require_once '../conn.php';

  $query = $_GET['query'];

  switch ($query) {
    case '*':

      $sql = "SELECT * FROM users";
      $result = mysqli_query($conn, $sql);
      if (mysqli_num_rows($result) > 0) {
        $users = array();
        while($row = mysqli_fetch_assoc($result)) {
		  unset($row['password']);

			//total de participantes
			$sql = "select count(*) as totalParticipants from participants p, users u where
					u.idUser = p.idUser
					and u.idUser = {$row["idUser"]}";
			$resultCount = mysqli_query($conn, $sql);
			$data = mysqli_fetch_assoc($resultCount);
			$row["totalParticipants"] = $data["totalParticipants"];

			//total que han resuelto
			$sql = "select count(*) as totalResolved from participants p, users u where
					u.idUser = p.idUser
					and p.resolved = 1
					and u.idUser = {$row["idUser"]}";
			$resultCountResolved = mysqli_query($conn, $sql);
			$data = mysqli_fetch_assoc($resultCountResolved);
			$row["totalResolved"] = $data["totalResolved"];



          array_push($users,$row);
        }
        $response->status = true;
        $response->users = $users;
        echo json_encode($response, JSON_NUMERIC_CHECK);
      } else {
		$response->status = false;
		$response->users = [];
        $response->message = "Aún no hay usuarios en la plataforma";
        echo json_encode($response);
      }

      break;
    case 'user':
      $user = $_GET['idUser'];
      $sql = "SELECT * FROM users WHERE idUser = '{$user}'";
      $result = mysqli_query($conn, $sql);
      if (mysqli_num_rows($result) > 0) {

        	//total de participantes
			$sql = "select count(*) as totalParticipants from participants p, users u where
      u.idUser = p.idUser
      and u.idUser = {$user}";
      $resultCount = mysqli_query($conn, $sql);
      $data = mysqli_fetch_assoc($resultCount);
      $row["totalParticipants"] = $data["totalParticipants"];

      //total que han resuelto
      $sql = "select count(*) as totalResolved from participants p, users u where
          u.idUser = p.idUser
          and p.resolved = 1
          and u.idUser = {$user}";
      $resultCountResolved = mysqli_query($conn, $sql);
      $data = mysqli_fetch_assoc($resultCountResolved);
      $row["totalResolved"] = $data["totalResolved"];


        $row = mysqli_fetch_assoc($result);
        unset($row['password']);
        $response->status = true;
        $response->userData = $row;
        echo json_encode($response, JSON_NUMERIC_CHECK);
      } else {
        $response->status = false;
        $response->message = "El usuario no existe";
        $response->sql = $sql;
        echo json_encode($response);
      }
      break;
  }
