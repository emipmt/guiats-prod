<?php

  require_once '../conn.php';
  require_once '../middleware.php';

  $idUser = $_POST['idUser'];
  $idUserDelete = $_POST['idUserDelete'];
	$res = verify("admin", $idUser, $conn);
	if(!$res["status"]) {
		echo json_encode($res);
		return;
	}

  $sql = "delete from users where iduser = '{$idUserDelete}'";

  if (mysqli_query($conn, $sql)) {
    $response->status = true;
    $response->message = "Se ha eliminado el usuario correctamente";
    echo json_encode($response);
  } else {
    $response->status = false;
    $response->message = "Ha ocurrido un error al eliminar el usuario";
    echo json_encode($response);
  }
