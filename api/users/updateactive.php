<?php

  require_once '../conn.php';
  require_once '../middleware.php';

  $active = $_POST['active'];
  $idUser = $_POST['idUser'];
  $idUserActive = $_POST['idUserActive'];

	$res = verify("admin", $idUser, $conn);
	if(!$res["status"]) {
		echo json_encode($res);
		return;
	}

  $sql = "UPDATE users SET active = {$active} WHERE idUser = '{$idUserActive}' ";
  if (mysqli_query($conn, $sql)) {
    $response->status = true;
    $response->message = "Se ha modificado el usuario correctamente";
    echo json_encode($response, JSON_NUMERIC_CHECK);
  } else {
    $response->status = false;
    $response->message = "Ha ocurrido un error al modificar usuario";
    echo json_encode($response);
  }
