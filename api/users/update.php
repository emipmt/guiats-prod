<?php

  require_once '../conn.php';
  require_once '../middleware.php';

  $idUser = $_POST["idUser"];
  $name = $_POST['name'];
  $orgName = $_POST["orgName"];
  $email = $_POST['email'];
  $phone = $_POST["phone"];
  $maxParticipants = $_POST["maxParticipants"];
  $type = $_POST["type"];
  $active = $_POST["active"];
  $idUserVerify = $_POST["idUserVerify"];

  $res = verify("admin", $idUserVerify, $conn);
	if(!$res["status"]) {
		echo json_encode($res);
		return;
	}


  $sqlVerifyAccount = "SELECT * FROM users WHERE email = '{$email}' ";
  $resultVerifyAccount = mysqli_query($conn, $sqlVerifyAccount);
  $rowVerifyAccount = mysqli_fetch_assoc($resultVerifyAccount);

  if (mysqli_num_rows($resultVerifyAccount) > 0 && $idUser != $rowVerifyAccount["idUser"]) {
	$response->status = false;
	$response->message = "Ya se está utilizando correo electrónico en otra cuenta";
	$response->flag = 1;
	echo json_encode($response);
  } else {
		$sql = "update users set name='{$name}', orgName='{$orgName}', email='{$email}', phone='{$phone}', maxParticipants='{$maxParticipants}'
			where idUser = {$idUser}";

		if (mysqli_query($conn, $sql)) {


			$_SESSION['id'] = $idUser;
			$_SESSION['name'] = $name;
			$_SESSION['orgName'] = $orgName;
			$_SESSION['email'] = $email;
			$_SESSION['phone'] = $phone;
			$_SESSION['maxParticipants'] = $maxParticipants;
			$_SESSION['type'] = $type;
			$_SESSION['active'] = $active;

			$response->status = true;
			$response->message = "Actualizado correctamente";
			$response->userData = $_SESSION;
			echo json_encode($response, JSON_NUMERIC_CHECK);

		} else {

			$response->status = false;
			$response->message = "Ha ocurrido un error al actualizar, cuenta intentalo más tarde.";
			echo json_encode($response);

		}
  }

?>