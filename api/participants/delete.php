<?php

  require_once '../conn.php';
  require_once '../middleware.php';

  $idParticipant = $_POST['idParticipant'];
  $idUser = $_POST['idUser'];
  $res = verify("client", $idUser, $conn);
	if(!$res["status"]) {
		echo json_encode($res);
		return;
	}

  $sql = "delete from participants where idParticipant = {$idParticipant}";
  if (mysqli_query($conn, $sql)) {
    $response->status = true;
    $response->message = "Se ha eliminado correctamente";
    echo json_encode($response);
  } else {
    $response->status = false;
    $response->message = "Ha ocurrido un error al eliminar";
    echo json_encode($response);
  }
