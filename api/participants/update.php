<?php

require_once '../conn.php';
require_once '../middleware.php';

$idParticipant = $_POST["idParticipant"];
$work = $_POST["work"];
$name = $_POST["name"];
$position = $_POST["position"];
$email= $_POST["email"];
$idUser= $_POST["idUser"];

$res = verify("client", $idUser, $conn);
if(!$res["status"]) {
	echo json_encode($res);
	return;
}

$sql = "update participants set work={$work}, name='{$name}', email='{$email}', position='{$position}' where idParticipant = {$idParticipant}";

if (mysqli_query($conn, $sql)) {
	$response->status = true;
	$response->message = "Actualizado correctamente";
	echo json_encode($response);

} else {
	$response->status = false;
	$response->message = "Ha ocurrido un error al actualizar dato, intentalo más tarde.";
	echo json_encode($response);

}

?>