<?php

require_once '../conn.php';
require_once '../middleware.php';

$participants = $_POST["participants"];
$idUser = $_POST["idUser"];
$res = verify("client", $idUser, $conn);
if(!$res["status"]) {
	echo json_encode($res);
	return;
}


foreach($participants as $idParticipant){
	$sql = "delete from participants where idParticipant = {$idParticipant}";
	if (mysqli_query($conn, $sql)) {
		$response->status = true;
		$response->message = "Se ha eliminado correctamente";
	} else {
		$response->status = false;
		$response->message = "Ha ocurrido un error al eliminar";
	}
}

echo json_encode($response);