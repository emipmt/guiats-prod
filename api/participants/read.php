<?php
require_once '../conn.php';

$idUser = $_POST["idUser"];
$getPersonalData = $_POST["personalData"];
$idParticipant = $_POST["idParticipant"];

$sql;

if ($idParticipant) {
	$sql = "SELECT idParticipant, work, name, email, position, password, resolved, emailSended FROM participants where  idParticipant = {$idParticipant}";
} else {
	$sql = "SELECT idParticipant, work, name, email, position, password, resolved, emailSended FROM participants where idUser = {$idUser}";
}

$result = mysqli_query($conn, $sql);
$data = array();

if (mysqli_num_rows($result) > 0) {

	while ($row = mysqli_fetch_assoc($result)) {
		if ($getPersonalData) {
			$sqlParticipant = "select B.name, C.optionType, C.optionName from selected A, option B, personaldataoptions C where B.idOption = A.idOption and C.idPersonalDataOptions = B.idPersonalDataOptions and idParticipant = {$row['idParticipant']}";
			$resultParticipant = mysqli_query($conn, $sqlParticipant);
			$personalData = array();

			if (mysqli_num_rows($resultParticipant) > 0) {
				while ($rowParticipant = mysqli_fetch_assoc($resultParticipant)) {
					$personalData[$rowParticipant['optionType']] =  array(
						"option" => $rowParticipant['name'],
						"optionName" => $rowParticipant['optionName']
					);
				}
			}

			$row["personalData"] = $personalData;
		}

		array_push($data, $row);
	}

	$response->status = true;
	$response->data = $data;
	echo json_encode($response, JSON_NUMERIC_CHECK);
} else {
	$response->status = false;
	$response->data = $data;
	$response->message = "Aún no hay participantes en la plataforma";
	echo json_encode($response);
}
