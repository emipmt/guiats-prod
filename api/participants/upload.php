 <?php

  require_once '../conn.php';
  require_once '../middleware.php';


function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'){
    $pieces = [];
    $max = mb_strlen($keyspace, '8bit') - 1;
    for ($i = 0; $i < $length; ++$i) {
        $pieces []= $keyspace[random_int(0, $max)];
    }
    return implode('', $pieces);
}

$target = "../uploads/";
$idUser = $_POST["idUser"];
$response->work = [];
$response->notNumbers = [];
$response->large = [];

$res = verify("client", $idUser, $conn);
if(!$res["status"]) {
	echo json_encode($res);
	return;
}

//crear directorio si no existe
if(!file_exists($target)){
	$err = mkdir($target, 0777);
}
$target = $target . "/" . random_str(20) . basename($_FILES["csv"]["name"]);

if (move_uploaded_file($_FILES["csv"]["tmp_name"], $target)) {
	$path = basename($target);

	$fp = fopen($target, "r") or die("Unable to open file!");
	//verificar el maximo de participantes
	$sql = "select maxParticipants from users where idUser = {$idUser}";
	$resultMax = mysqli_query($conn, $sql);
	$data = mysqli_fetch_assoc($resultMax);

	$num = 0;
	$line;
	while (!feof ($fp)) {
		if ($linea = fgets($fp)){
		$num++;
		}
	}
	fseek($fp, 0);
	//cabeceras del csv
	$line = fgets($fp);

	if($num - 1 > $data["maxParticipants"]){
		$response->status = false;
    	$response->message = "El archivo excede el tu límite de participantes, contacta a human express para aumentar tu límite";
    	echo json_encode($response);
	} else {
		while(!feof($fp)){
			$line = fgets($fp);
			$col = explode(",", $line);
			//col0 work, col1 name, col2 email, col3 position	

			//verificar que es menor a 8
			if(strlen($col[0]) > 8) {
				array_push($response->large, $col[0]);
				continue;
			}

			//introducir datos a DB
			$password = substr(md5($col[1]), 0, 3) . rand(10, 99) . date('Hs') . rand(1, 9);
			$col[2] = trim($col[2]);
			if(strlen($col[2]) == 0){ //no tiene correo
				$col[2] = "0";
			} 

			$sql = "insert into participants (work, name, email, position, password, idUser) values
			('{$col[0]}', '{$col[1]}', '{$col[2]}' ,'{$col[3]}', '{$password}', {$idUser})";

			if(preg_match("/^\d*$/", $col[0])){
				if (!mysqli_query($conn, $sql)) {
					if(mysqli_errno($conn) == 1062){
						$response->status = false;

						array_push($response->work, $col[0]);
						$response->message = "Los participantes duplicados fueron ignorados.";

					} else {
						$response->status = false;
						$response->message = "Ha ocurrido un error al crear el registro";

					}
				} else {
					$response->status = true;
					$response->message = "Guardado correctamente";

				}
			} else {
				$response->status = false;
				$response->message = "Asegurate de que el número de trabajador son solo números";
				array_push($response->notNumbers, $col[0]);
			}

		}
		fclose($fp);
		unlink($target);

		echo json_encode($response);
	}



} else {
	$response->status = false;
    $response->message = "Ha ocurrido un error al subir el documento";
    echo json_encode($response);
}

