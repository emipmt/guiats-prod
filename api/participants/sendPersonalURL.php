<?php

require_once '../conn.php';

$personalURL = $_POST["personalURL"];

$idParticipant = $_POST["idParticipant"];
$work = $_POST["work"];
$email = $_POST["email"];
$position = $_POST["position"];
$clientName = $_POST["clientName"];
$clientEmail = $_POST["clientEmail"];
$participantName = $_POST["participantName"];
$password = $_POST["password"];
$phone = $_POST["phone"];




if(sendEmail($work,$email,$position,$clientName,$participantName,$password,$phone, $personalURL,$clientEmail)){

   $sql = "update participants set emailSended=1 where idParticipant = {$idParticipant}";

   if (mysqli_query($conn, $sql)) {
      $response->status = true;
      $response->message = "Correo electrónico enviado.";
      echo json_encode($response);
   } else{
      $response->status = false;
      $response->message = "Correo electrónico enviado, pero no se pudo actualizar la información.";
      echo json_encode($response);
   }

 
} else {
   $response->status = false;
	$response->message = "No se pudo enviar el correo electrónico";
	echo json_encode($response);
}



function sendEmail ($work,$email,$position,$clientName,$participantName,$password,$phone, $personalURL,$clientEmail) {
	//guiats.human-express.com/#/cuestionario/${idCliente}/${numeroDeTrabajador}/${contraseña}
    $message = "
   
   <div style='max-width:100vw'>
   <div style='background: url(https://guiats.human-express.com/img/background.e380e90d.png); background-size:100%;background-position:center; width:100%;padding:5% 0'>
      .
      <div style='text-align:center;'>
         <img src='https://guiats.human-express.com/img/logo.1b957308.png' style='width:100px;' />
      </div>
   </div>
   <div style='padding:5%;'>
      <p>
         Numero de Empleado: {$work}  <br />
         Nombre: {$participantName} <br />
         Puesto: {$position} <br />
         Clave de Acceso: {$password} <br /><br />
         Favor de dar Clic en el siguiente enlace para resolver el Cuestionario GUIATS <br>
         <br>
         <a href='{$personalURL}' target='blank'>{$personalURL}</a>
         <br>
         <br>
         Accesar con su Numero de Empleado y Clave de Acceso: {$password}
         <br>
         <br>
         Cualquier duda acudir a Recursos Humanos o llamar al {$phone}
      </p>
   </div>
";

    $message = utf8_decode($message);

    $title = "Liga para resolver el Cuestionario GUIATS {$clientName}";
    
    $title = "=?ISO-8859-1?B?".base64_encode($title)."=?=";
    $headers = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
    $headers .= "From: {$clientEmail}";

    $mail = mail($email,$title,$message,$headers);
    if($mail){
      return true;
    } else{
      return false;
    }
  };