<?php

require_once '../conn.php';
require_once '../middleware.php';
header('Content-Type: text/csv');
header('Content-Disposition: attachment; filename="sample.csv"');

$idUser = $_POST["idUser"];
$res = verify("client", $idUser, $conn);

if(!$res["status"]) {
	echo json_encode($res);
	return;
}

$sql = "SELECT idParticipant, resolved, work, name, email, position FROM participants WHERE idUser = {$idUser}";

$data = array();
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
	while($row = mysqli_fetch_assoc($result)) {

		//extraer opciones
		$row["options"] = [];
		$sqlOptions = "SELECT pd.optionName, o.name from personaldataoptions pd, selected s, option o, participants p
			where s.idParticipant = p.idParticipant
			and s.idOption = o.idOption
			and o.idPersonalDataOptions = pd.idPersonalDataOptions
			and p.idParticipant = {$row['idParticipant']}";

		$resultOptions = mysqli_query($conn, $sqlOptions);

		while($rowOption = mysqli_fetch_assoc($resultOptions)){
			array_push($row["options"], $rowOption);
		}

		//extraer respuestas
		$row["answers"] = [];
		$sqlAnswers = "SELECT a.answer, r.title from answers a, participants p, reactives r
						WHERE p.idParticipant = a.idParticipant
						and a.idReactive = r.idReactive
						and p.idParticipant = {$row['idParticipant']}";

		$resultAnswers = mysqli_query($conn, $sqlAnswers);

		while($rowAnswer = mysqli_fetch_assoc($resultAnswers)){
			array_push($row["answers"], $rowAnswer);
		}

		array_push($data,$row);
	}

	//agregarlos al csv
	$fp = fopen('php://output', 'wb');

	foreach($data as $element){

		$line = [];
		array_push($line, $element["resolved"]);
		array_push($line, $element["work"]);

		foreach($element["answers"] as $answer){
			array_push($line, $answer["answer"]);
		}


		array_push($line, $element["name"]);
		array_push($line, $element["email"]);

		foreach($element["options"] as $option){
			array_push($line, $option["name"]);
		}

		fputcsv($fp, $line);
	}

	fclose($fp);
	/*
	$response->status = true;
	$response->data = $data[0];
	//var_dump($response);
	echo json_encode($response, JSON_NUMERIC_CHECK);
	*/


} else {
	$response->status = false;
	$response->data = $data;
	$response->message = "No hay datos con ese usuario";
	echo json_encode($response);
}
