<?php

require_once '../conn.php';
require_once "../middleware.php";

$name = $_POST["name"];
$work = $_POST["work"];
$email = $_POST["email"];
$position = $_POST["position"];
$idUser = $_POST["idUser"];

$res = verify("client", $idUser, $conn);
if(!$res["status"]) {
	echo json_encode($res);
	return;
}

if(strlen($work) > 8) {
	$response->status = false;
	$response->message = "Número debe ser menor a 8 caracteres";
	echo json_encode($response);
	return;
}

$password = substr(md5($name), 0, 3) . rand(10, 99) . date('Hs') . rand(1, 9);

//maximo de participantes
$sql = "select maxParticipants from users where idUser = {$idUser}";
$resultMax = mysqli_query($conn, $sql);
$maxParticipants = mysqli_fetch_assoc($resultMax)["maxParticipants"];

//total de participantes
$sql = "select count(*) as totalParticipants from participants p, users u where
u.idUser = p.idUser
and u.idUser = {$idUser}";
$resultCount = mysqli_query($conn, $sql);
$totalParticipants = mysqli_fetch_assoc($resultCount)["totalParticipants"];


if($totalParticipants < $maxParticipants){

	$sql = "insert into participants (work, name, email, position, password, idUser) values
		('{$work}', '{$name}', '{$email}', '{$position}', '{$password}', {$idUser})";

	if(preg_match("/^\d*$/", $work)){
		if (!mysqli_query($conn, $sql)) {
			if(mysqli_errno($conn) == 1062){
				$response->status = false;
				$response->message = "El número ya existe";
				echo json_encode($response);
			} else {
				$response->status = false;
				$response->message = "Ha ocurrido un error al crear";
				echo json_encode($response);
			}
		} else {
			$response->status = true;
			$response->message = "Guardado correctamente";
			echo json_encode($response);
		}
	} else {
		$response->status = false;
		$response->message = "Asegurate de que el número de trabajador son solo números";
		echo json_encode($response);
	}
} else {
	$response->status = false;
	$response->message = "Número de empleados alcanzados, contacta a Human Express para aumentar tu límite";
    echo json_encode($response);
}
	
?>
