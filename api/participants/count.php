<?php
require_once '../conn.php';

$idUser = $_POST["idUser"];

$sql = "SELECT count(*) as total FROM participants where idUser = '{$idUser}'";
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
	$response->status = true;
	$response->data = mysqli_fetch_assoc($result);
	echo json_encode($response, JSON_NUMERIC_CHECK);
} else {
	$response->status = false;
	$response->data = 0;
	$response->message = "Aún no hay usuarios en la plataforma";
	echo json_encode($response);
}
