<?php

require_once './conn.php';


//SELECT resolved, work, name, email FROM participants WHERE idUser = 1

/*

optiones personales
SELECT pd.optionName, o.name from personaldataoptions pd, selected s, option o, participants p
where s.idParticipant = p.idParticipant
and s.idOption = o.idOption
and o.idPersonalDataOptions = pd.idPersonalDataOptions
and p.idUser = 1
*/

/*
respuestas
SELECT a.answer, p.name from answers a, participants p
WHERE p.idParticipant = a.idParticipant
and p.idUser = 1

//respuestas con titulo
SELECT a.answer, r.title, p.name from answers a, participants p, reactives r
WHERE p.idParticipant = a.idParticipant
and a.idReactive = r.idReactive
and p.idUser = 1

*/

/*
$sql = "SELECT a.answer, p.name from answers a, participants p
WHERE p.idParticipant = a.idParticipant
and p.idUser = {$idUser}";*/


$idUser = 1;
$sql = "SELECT idParticipant, resolved, work, name, email FROM participants WHERE idUser = {$idUser}";

$data = array();
$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) {
	while($row = mysqli_fetch_assoc($result)) {

		//extraer opciones
		$row["options"] = [];
		$sqlOptions = "SELECT pd.optionName, o.name from personaldataoptions pd, selected s, option o, participants p
			where s.idParticipant = p.idParticipant
			and s.idOption = o.idOption
			and o.idPersonalDataOptions = pd.idPersonalDataOptions
			and p.idParticipant = {$row['idParticipant']}";

		$resultOptions = mysqli_query($conn, $sqlOptions);

		while($rowOption = mysqli_fetch_assoc($resultOptions)){
			array_push($row["options"], $rowOption);
		}

		//extraer respuestas
		$row["answers"] = [];
		$sqlAnswers = "SELECT a.answer, r.title from answers a, participants p, reactives r
						WHERE p.idParticipant = a.idParticipant
						and a.idReactive = r.idReactive
						and p.idParticipant = {$row['idParticipant']}";

		$resultAnswers = mysqli_query($conn, $sqlAnswers);

		while($rowAnswer = mysqli_fetch_assoc($resultAnswers)){
			array_push($row["answers"], $rowAnswer);
		}

		array_push($data,$row);
	}

	$response->status = true;
	$response->data = $data;
	//var_dump($response);
	echo json_encode($response, JSON_NUMERIC_CHECK);
} else {
	$response->status = false;
	$response->data = $data;
	$response->message = "No hay datos con ese usuario";
	echo json_encode($response);
}







/*
middleware
require_once "./middleware.php";

$response = verify("client", 3, $conn);
if(!$response["status"]) return;

echo "access ok";*/