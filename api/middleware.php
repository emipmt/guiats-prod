<?php

function verify($type, $idUser, $conn){
	$response = [];

	$sql = "select idUser, type, active from users where idUser = {$idUser}";

	$result = mysqli_query($conn, $sql);
	$data = mysqli_fetch_assoc($result);

	if($type == "admin"){
		//El usuario existe es de tipo admin y esta activo

		if(!empty($data["idUser"]) && $data["type"] == 1 && $data["active"]){
			$response["status"] = true;
			$response["message"] = "Tienes acceso";
		} else {
			$response["status"] = false;
			$response["message"] = "No tienes acceso. Por favor cierra sesión";
		}
	} else {
		//El usuario existe es de tipo client y esta activo
		if(!empty($data["idUser"]) && ($data["type"] == 0 || $data["type"] == 1) && $data["active"]){
			$response["status"] = true;
			$response["message"] = "Tienes acceso";
		} else {
			$response["status"] = false;
			$response["message"] = "No tienes acceso. Por favor cierra sesión";
		}
	}
	return $response;
}